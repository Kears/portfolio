# Guide to Parmigiano-Reggiano


Parmigiano-Reggiano is an Italian cheese named after the Italian provinces of
Parma and Reggio Emilia where it originates. This guide explains how to
identify, purchase, store and prepare parmigiano.

## Identification

American shoppers can use European Union food labels to identify real Italian
products from imitations. Italy uses the *Denominazione d'Origine Protetta*
(DOP) food label to identify foods produced in specific regions of Italy
using specific traditional techniques. DOP food labels feature a 
[red and yellow DOP logo][DOP]. 

Italy uses a DOP food label to identify and protect authentic parmigiano.
American imitations go by the name parmesan, which is actually the French
name for parmigiano even though the cheese is Italian. Authentic parmigiano
is produced only in northern Italy and is available in the United States only
as an import. Dishes that use parmigiano include fettuccine alfredo, the
Americanized version of the Italian dish called fettuccine al burro.

You can identify authentic parmigiano by its DOP food label and by its full
name, Parmigiano-Reggiano, branded into its outer shell or
[rind][parmigiano-rind]. Small to medium size wedges of parmigiano will only 
show several letters of its name. Imitations often have an unmarked, pale rind
because their makers only age them for a short time, not the twelve months
required for DOP certification. Some imitations sold as wedges do not include
a rind at all. Pre-grated and pre-sliced cheeses also omit the rind. 

## Purchasing

The storage requirements for cheese influence the form in which you should
purchase it. Excessive exposure to air causes cheese to dry out which leads
to a loss of taste. For this reason, you should buy wedges of cheese instead
of pre-grated or pre-sliced cheese. Those forms of cheese dry out more
rapidly because they expose a greater surface area to air circulation. 

## Storage

To reiterate, excessive exposure to air causes cheese to dry out which leads
to a loss of taste. On the other hand, insufficient air circulation causes
moisture to accumulate which can spoil the cheese. Store cheese by
refrigerating it wrapped in wax paper, parchment paper or specialty cheese
paper. Store cheese in this manner for up to two weeks or a month. Replace
the wrap every one to two weeks. Avoid airtight plastic wrap. 

## Preparation

Before you use it, take refrigerated cheese out of the refrigerator and bring
it to room temperature. Do so by leaving it wrapped outside the refrigerator
for at least one hour. Larger pieces of cheese require more time to warm up.
Letting cheese warm up improves its ability to impart its flavor. 

For the best taste, grate cheese immediately before you start cooking. Grating
cheese too far ahead of time causes it to dry out and lose its taste. Use a
grating panel or box to grate cheese. Do so over a rimmed baking sheet to
avoid spilling grated cheese onto the counter or floor. Proper storage makes
it easier to grate cheese because dry cheese hardens, making the cheese
difficult to grate. 


[DOP]:https://bitbucket.org/Kears/portfolio/raw/b21aaf0026e210367dc2f0323fa3d0c446f8efcd/dop.png
[parmigiano-rind]:https://bitbucket.org/Kears/portfolio/raw/44590ad0ed4928e1e858c6a546bd3312e54e6fd5/parmigiano-rind.jpg
