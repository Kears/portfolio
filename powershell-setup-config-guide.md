# PowerShell Setup & Configuration Guide


## Introduction

This guide explains how to setup and configure PowerShell 7 on Windows 10. By
default, Windows blocks users from running PowerShell scripts. This guide
explains how to turn off that restriction. This guide also explains how to
use a profile to configure PowerShell and provides recommendations about what
configuration changes to make. Those changes focus on how PowerShell handles
errors. Specifically, they involve making certain silent errors explicit and
making PowerShell halt the execution of a script when encountering an error.


## Enable the Execution of PowerShell Scripts

By default, Windows enforces an [execution policy][execution policies] that
blocks users from running PowerShell scripts. Under this policy, users can
only execute PowerShell commands in an interactive terminal. According to its
documentation, the execution policy is "a safety feature" to help "prevent
the execution of malicious scripts." Developers and IT professionals who need
to run PowerShell scripts can lift this restriction by following the steps
below.


### 1. Choose a Permissive Execution Policy

Change the active execution policy from `Restricted` to a more permissive
execution policy from this [list][execution policies]. Specifically, choose
an execution policy that allows you to run PowerShell scripts.

I recommend using the `RemoteSigned` [execution policy][remotesigned]. This
execution policy allows users to run PowerShell scripts they create locally
but blocks scripts downloaded from the internet with two exceptions. The
first exception applies to scripts with a digital signature from a trusted
publisher. The second exception applies to scripts without a digital
signature that the user manually unblocks using the `Unblock-File` 
[command][unblock-file].


### 2. Start PowerShell With Administrator Privileges

Start an instance of a PowerShell interactive terminal with administrator
privileges. To do so, right click the PowerShell application and select
"Run as Administrator" from the drop-down menu. 


### 3. Set the Permissive Execution Policy

At the PowerShell interactive terminal, use the `Set-ExecutionPolicy`
[command][set-executionpolicy] to set the execution policy you chose. The 
example below sets the execution policy to `RemoteSigned` for the current
user:

```Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser```

To set a different execution policy, replace `RemoteSigned` with the name of
the other execution policy. After you type the command, press enter to
execute the command. 


### 4. Verify the Execution Policy Is Set Correctly

Use the `Get-ExecutionPolicy` [command][get-executionpolicy] to verify the
active execution policy. You do not need administrator privileges to execute
this command. Invoke the command as follows:

```Get-ExecutionPolicy```

Executing this command returns the name of the active execution policy for the
current user. If the name matches the name of the execution policy you
attempted to set in step three, you have succeeded in changing the execution
policy.

[execution policies]:https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_execution_policies?view=powershell-7.2
[execution policy values]:https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_execution_policies?view=powershell-7.2#powershell-execution-policies
[remotesigned]:https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_execution_policies?view=powershell-7.2#remotesigned
[unblock-file]:https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/unblock-file?view=powershell-7.2
[set-executionpolicy]:https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.security/set-executionpolicy?view=powershell-7.2
[get-executionpolicy]:https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.security/get-executionpolicy?view=powershell-7.2


## Use a Profile to Configure PowerShell

A PowerShell [profile][] is a PowerShell script that automatically runs every
time PowerShell starts. You can use this script to configure PowerShell by
adding configuration commands to it. This section of the guide explains how
to configure PowerShell using a profile. This section also provides
recommendations about how to configure PowerShell.

To create a PowerShell profile, first create a blank PowerShell script with
the name and extension `Profile.ps1`. In Windows File Explorer, navigate to
the hard drive containing your Windows installation, typically `C:\`. Open
the `Users` folder. Open the folder named after the current user. Open the
`Documents` folder. Open the `PowerShell` folder if it exists. If it does
not, create it and open it. Finally, place the `Profile.ps1` script in the
`PowerShell` folder. The full path to the file at its destination should look
something like this:

```C:\Users\JohnSmith\Documents\PowerShell\Profile.ps1```

The full path on your machine will have your user name instead of `JohnSmith`.
Placing a PowerShell script at that location with that name and extension will
make PowerShell recognize it as a profile.

[profile]:https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_profiles?view=powershell-7.2


## Convert Silent Errors to Explicit Errors

Many developers prefer explicit errors over silent errors. For instance, the
[Zen of Python][] recommends that "Errors should never pass silently; Unless
explicitly silenced." Explicit errors notify developers of bugs and mistakes.
On the other hand, silent errors do not, forcing developers to make additional
efforts to find those bugs and mistakes.

PowerShell provides an optional [strict mode][] that, when enabled, transforms
certain silent errors into explicit errors. Add the following command to a
separate line in your PowerShell profile to always enable strict mode:

```Set-StrictMode -Version Latest```

PowerShell does not have a command to verify whether strict mode is enabled.
As of PowerShell 7, the latest version of strict mode is 3.0. Enabling this
version of strict mode transforms the following silent errors into explicit
ones.


### 1. Referencing Uninitialized Variables

Referencing an uninitialized variable occurs when you try to use a variable
before assigning it a value. When strict mode is off, accessing an
uninitialized variable gives you `$null` without throwing an error. An
uninitialized variable of a numeric type behaves differently. When strict
mode is off, accessing its value gives you `0` without throwing an error.
Accessing an uninitialized variable with strict mode enabled always throws an
explicit error.


### 2. Referencing Non-existing Object Properties

Referencing a non-existing object property occurs when you use a name to
access an object property but nothing matches the name. This commonly occurs
due to spelling errors and when changing a property name. When strict mode is
off, referencing a non-existing object property gives you `$null` without
throwing an error. Enabling strict mode causes this mistake to throw an
explicit error instead.


### 3. Using Invalid Array Indexes

An array is a container for storing objects. To access an object in an array
you use the object's index, the integer specifying the object's position in
the array. Array indexes start from the integer `0`. Thus, you would use the
indexes `0`, `1`, and `2` to access the first, second and third objects
respectively in an array. An invalid array index is an integer that does not
correspond to the position of any object in an array. For instance, if an
array contains three objects, the index `3` would be invalid because it would
refer to a non-existing fourth object.

When strict mode is off, using an invalid index to access an object in an
array gives you `$null` without throwing an error. Enabling strict mode
causes this mistake to throw an explicit error instead.


### 4. Calling Functions Using Method Call Syntax

A function is a container for a sequence of commands. A method is also a
container for a sequence of commands but resides and operates on an object.
Both functions and methods can accept arguments to customize their behavior.
In PowerShell, you use parentheses to call a method and pass arguments to it.
Below is an example of a method call with an argument:

```[Console]::WriteLine('hello world')```

In PowerShell, you call a function and pass arguments to it without
parentheses. Below is an example of a function call:

```Write-Host 1, 2, 3```

When strict mode is off, PowerShell allows you to call a function using
parentheses and enclose function arguments in parentheses. Enabling strict
mode transforms this into a syntax error. For example, the following would
now raise an error:

```Write-Host(1, 2, 3)```


[Zen of Python]:https://www.python.org/dev/peps/pep-0020/
[strict mode]:https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/set-strictmode?view=powershell-7.2


## Terminate Scripts When Errors Occur

By default, PowerShell tries to continue executing a script after encountering
an error. On the other hand, you can configure PowerShell to terminate a
script when encountering an error. Doing so prevents the error from inflicting
unintended side effects on your project or system after the error is caught.

PowerShell uses the `$ErrorActionPreference` 
[preference variable][error action preference] to configure how PowerShell 
reacts to errors. [Preference variables][preference variables] are global 
variables that you set using the assignment operator. To configure PowerShell
to always terminate upon an error, change the `$ErrorActionPreference` value
from `Continue` to `Stop`. You can do so by adding the following line to
your PowerShell profile:

```$ErrorActionPreference = 'Stop'```

To verify the change, start a PowerShell interactive terminal and type
`$ErrorActionPreference`. This will retrieve the variable's value to tell you
the currently active error action preference (without the quotes).

[preference variables]:https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_preference_variables?view=powershell-7.2
[error action preference]:https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_preference_variables?view=powershell-7.2#erroractionpreference
